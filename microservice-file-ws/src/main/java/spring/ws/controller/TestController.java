package spring.ws.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by f.putra on 1/20/17.
 */
@RestController
public class TestController extends BaseController{

    @RequestMapping(value = "/hello", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Map<String, Object> printHello(ModelMap model) {
        model.addAttribute("user", "Hello Spring MVC Framework!");
        model.addAttribute("mbul", "asu");
        return convertModel(model, HttpStatus.OK);
    }
}
