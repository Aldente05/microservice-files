package spring.ws.controller;

import com.spring.core.common.Result;
import com.spring.core.entity.Document;
import com.spring.core.entity.DocumentMetaData;
import com.spring.core.service.ArchiveService;
import com.spring.core.service.SystemParameterService;
import com.spring.core.util.ImageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.Map;

/**
 * Created by f.putra on 10/20/16.
 */
@RestController
public class SystemController extends BaseController {
//    private static final Logger LOG = Logger.getLogger(ArchiveController.class);

    @Autowired
    private SystemParameterService systemParameterService;
    @Autowired
    private ImageHelper imageHelper;
    @Autowired
    private ArchiveService archiveService;

    @RequestMapping(value = "/image", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Map<String, Object> image(ModelMap model) {
        model.addAttribute("data", systemParameterService.getImage());
        return convertModel("data", model);
    }


    /**
     * Adds a document to the archive.
     * <p>
     * Url: /archive/upload?file={file}&person={person}&date={date} [POST]
     *
     * @param file       A file posted in a base64 request
     * @return The meta data of the added document
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public
    @ResponseBody
    DocumentMetaData handleFileUpload(
            @RequestParam(value = "file") MultipartFile file) {
        try {
                Document document = new Document(file.getBytes(), file.getOriginalFilename(), new Date(), 1);
               archiveService.save(document);
               return document.getMetadata();
        } catch (RuntimeException e) {
//            LOG.error("Error while uploading.", e);
            throw e;
        } catch (Exception e) {
//            LOG.error("Error while uploading.", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Finds document in the archive. Returns a list of document meta data
     * which does not include the file data. Use getDocument to get the file.
     * Returns an empty list if no document was found.
     * <p>
     * Url: /archive/documents?person={person}&date={date} [GET]
     *
     * @param websiteId The name of the uploading person
     * @return A list of document meta data
     */
    @RequestMapping(value = "/documents", method = RequestMethod.GET)
    public Map<String, Object> findDocument(
            @RequestParam(value = "website_id", required = false) int websiteId,
            @RequestParam(value = "offset") int offset,
            @RequestParam(value = "limit") int limit) {
        return convertModel(archiveService.findDocuments(websiteId, offset, limit), HttpStatus.OK);
    }

    /**
     * Returns the document file from the archive with the given UUID.
     * <p>
     * Url: /archive/document/{id} [GET]
     *
     * @param id The UUID of a document
     * @return The document file
     */
    @RequestMapping(value = "/document/{id}", method = RequestMethod.GET)
    public HttpEntity<byte[]> getDocument(@PathVariable String id) {
        // send it back to the client
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.IMAGE_JPEG);
        return new ResponseEntity<byte[]>(archiveService.getDocumentFile(id), httpHeaders, HttpStatus.OK);
    }
}
