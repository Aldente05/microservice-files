/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.core;

/**
 * @author krissadewo
 */
public class Profile {

    private String uploadLocation;
    private String name;
    private String appTitle;
    private String imageDir;

    public String getUploadLocation() {
        return uploadLocation;
    }

    public void setUploadLocation(String uploadLocation) {
        this.uploadLocation = uploadLocation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppTitle() {
        return appTitle;
    }

    public void setAppTitle(String appTitle) {
        this.appTitle = appTitle;
    }

    public String getImageDir() {
        return imageDir;
    }

    public void setImageDir(String imageDir) {
        this.imageDir = imageDir;
    }

    //TODO  rename this value with encripted string as possibble, this is to be easy for someone to hack via application context
    public static enum STATUS {
        PROD, DEV

    }
}
