/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.core.dao.impl;

import com.spring.core.dao.UserDAO;
import com.spring.core.entity.Role;
import com.spring.core.entity.User;
import com.spring.core.util.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author krissadewo
 */
@Repository
public class UserDAOImpl implements UserDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public User save(final User entity) {
        final String sql = "INSERT INTO " + Table.SYSTEM_USER + "(" +
                "username, " +
                "realname, " +
                "password, " +
                "role_id) " +
                "VALUES(?,?,?,?) ";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(Connection -> {
            PreparedStatement ps = Connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, entity.getUsername());
            ps.setString(2, entity.getRealname());
            ps.setString(3, entity.getPassword());
            ps.setLong(4, entity.getRole().getId());
            return ps;
        });

        entity.setId(keyHolder.getKey().longValue());

        return entity;
    }

    @Override
    public User saveLogin(User entity) {
        String sql = "INSERT INTO " + Table.LOG_USER + " (user_id, session, time_login) "
                + "VALUES(?,?,?) ";

        jdbcTemplate.update(sql,
                entity.getId(),
                entity.getSession(),
                entity.getTimeLogin());

        return entity;
    }

    @Override
    public User saveLogout(User entity) {
        String sql = "UPDATE " + Table.LOG_USER + " "
                + "SET time_logout = ? "
                + "WHERE id = ? ";

        jdbcTemplate.update(sql,
                entity.getTimeLogout(),
                entity.getIdLogUser());

        return entity;

    }

    @Override
    public User update(User entity) {
        String sql = "UPDATE " + Table.SYSTEM_USER + " "
                + "SET username = ?,"
                + "password = ? ,"
                + "role_id =  ?,"
                + "realname = ?  "
                + "WHERE id = ? ";

        jdbcTemplate.update(sql,
                entity.getUsername(),
                entity.getPassword(),
                entity.getRole().getId(),
                entity.getRealname(),
                entity.getId());
        return entity;
    }

    @Override
    public User delete(User entity) {
        String sql = "UPDATE " + Table.SYSTEM_USER
                + "SET is_delete = 'Y',"
                + "delete_time = ?,"
                + "delete_by = ? "
                + "WHERE id = ? ";

        jdbcTemplate.update(sql,
                entity.getDeleteTime(),
                entity.getDeleteBy(),
                entity.getId());
        return entity;
    }

    public List<User> find(User param, Integer offset, Integer limit) {
        String sql = "SELECT * FROM " + Table.SYSTEM_USER + " u "
                + "INNER JOIN " + Table.SYSTEM_ROLE + " r ON r.id = u.role_id "
                + "WHERE is_delete = 'N' ";

        return jdbcTemplate.query(sql, new UserRowMapper());

    }

    @Override
    public User findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User findByParam(int user_id, int tryout_id) {
        return null;
    }

    @Override
    public List<User> find(Integer offset, Integer limit) {
        return null;
    }

    @Override
    public int count(User entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public User getByUsername(String username) {
        String sql = "SELECT * FROM " + Table.SYSTEM_USER + " u "
                + "INNER JOIN " + Table.SYSTEM_ROLE + " r ON r.id = u.role_id "
                + "WHERE username = ? "
                + "AND is_delete = 'N' ";

        return jdbcTemplate.queryForObject(sql, new Object[]{username}, new UserRowMapper());
    }

    @Override
    public User getLogUser(String session) {
        String sql = "SELECT * "
                + "FROM log_user "
                + "WHERE session = ? LIMIT 1 ";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{session}, new LogRowMapper());
        } catch (EmptyResultDataAccessException dae) {
            return null;
        }
    }

    @Override
    public User getLogUser(Long id) {
        String sql = "SELECT *"
                + " FROM log_user "
                + "WHERE user_id = ? "
                + "ORDER BY time_login DESC LIMIT 1";

        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new LogRowMapper());
    }

    @Override
    public String getPassword(User user) {
        String sql = "SELECT password FROM " + Table.SYSTEM_USER
                + "WHERE id =  ? "
                + "AND is_delete = 'N' ";

        return jdbcTemplate.queryForObject(sql, new Object[]{user.getId()}, String.class);
    }

    class UserRowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setId(rs.getLong("id"));
            user.setRealname(rs.getString("real_name"));
            user.setUsername(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            user.setTimeLogin(new Date());

            Role role = new Role();
            role.setId(rs.getLong("role_id"));
            role.setNama(rs.getString("nama"));
            user.setRole(role);
            return user;
        }
    }

    class LogRowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setIdLogUser(rs.getLong("id"));
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            user.setId(rs.getLong("user_id"));
            try {
                user.setTimeLogin(dateFormat.parse(rs.getString("time_login")));
            } catch (ParseException ex) {
                Logger.getLogger(UserDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            return user;
        }
    }
}
