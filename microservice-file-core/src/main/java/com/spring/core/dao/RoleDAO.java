/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.core.dao;


import com.spring.core.entity.Role;

/**
 * @author krissadewo
 */
public interface RoleDAO extends BaseDAO<Role> {

}
