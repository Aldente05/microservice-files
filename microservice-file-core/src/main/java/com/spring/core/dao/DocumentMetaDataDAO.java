package com.spring.core.dao;

import com.spring.core.entity.DocumentMetaData;

import java.util.List;

/**
 * Created by godfather on 1/31/17.
 */
public interface DocumentMetaDataDAO extends BaseDAO<DocumentMetaData> {

    List<DocumentMetaData> findByWebsiteId(int websiteId, int offset, int limit);
}
