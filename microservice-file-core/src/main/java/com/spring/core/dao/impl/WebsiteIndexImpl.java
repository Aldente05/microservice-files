package com.spring.core.dao.impl;

import com.spring.core.dao.WebsiteIndexDAO;
import com.spring.core.entity.User;
import com.spring.core.entity.WebsiteIndex;
import com.spring.core.util.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by godfather on 1/31/17.
 */
@Repository
public class WebsiteIndexImpl implements WebsiteIndexDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public WebsiteIndex save(WebsiteIndex entity) {
        return null;
    }

    @Override
    public WebsiteIndex update(WebsiteIndex entity) {
        return null;
    }

    @Override
    public WebsiteIndex delete(WebsiteIndex entity) {
        return null;
    }

    @Override
    public WebsiteIndex findById(Long id) {
        return null;
    }

    @Override
    public WebsiteIndex findByParam(int user_id, int tryout_id) {
        return null;
    }

    @Override
    public List<WebsiteIndex> find(Integer offset, Integer limit) {
        return null;
    }

    @Override
    public int count(WebsiteIndex param) {
        return 0;
    }

    @Override
    public WebsiteIndex getByUsername(String Username) {
        String sql = "SELECT * FROM " + Table.SYSTEM_USER +
                " INNER JOIN " + Table.WEBSITE_INDEX  +
                " r ON r.owner_id = u.id " +
                " WHERE email = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{Username}, new WebsiteRowMapper());
    }

    class WebsiteRowMapper implements RowMapper<WebsiteIndex> {

        @Override
        public WebsiteIndex mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setId(rs.getLong("id"));
            user.setRealname(rs.getString("real_name"));
            user.setUsername(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            user.setTimeLogin(new Date());

            WebsiteIndex role = new WebsiteIndex();
            role.setId(rs.getInt("id"));
            role.setWebsiteId(rs.getLong("website_id"));
            role.setWebsiteName(rs.getString("nama_website"));
            return role;
        }
    }
}
