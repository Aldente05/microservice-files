package com.spring.core.dao;

import com.spring.core.entity.WebsiteIndex;

/**
 * Created by godfather on 1/31/17.
 */
public interface WebsiteIndexDAO extends BaseDAO<WebsiteIndex>{

    WebsiteIndex getByUsername(String Username);
}
