package com.spring.core.dao;

import com.spring.core.entity.Document;
import com.spring.core.entity.DocumentMetaData;

import java.util.Date;
import java.util.List;

/**
 * Created by f.putra on 10/20/16.
 */
public interface DocumentDAO {
    /**
     * Inserts a document in the data store.
     *
     * @param document A DocumentHelper
     */
    void insert(Document document);

    /**
     * Finds documents in the data store matching the given parameter.
     * A list of document meta data is returned which does not include the file data.
     * Use load and the id from the meta data to get the document file.
     * Returns an empty list if no document was found.
     *
     * @param websiteId The website of a id
     * @param offset Data Start
     * @return A list of document meta data
     */
    List<DocumentMetaData> findByWebsiteId(int websiteId, int offset, int limit);

    /**
     * Returns the document from the data store with the given id.
     * The document file and meta data is returned.
     * Returns null if no document was found.
     *
     * @param uuid The id of the document
     * @return A document incl. file and meta data
     */
    Document load(String uuid);
}
