package com.spring.core.dao;

import com.spring.core.entity.Document;
import com.spring.core.entity.DocumentMetaData;

import java.util.Date;
import java.util.List;

/**
 * Created by f.putra on 10/20/16.
 */
public interface ArchiveDAO {
     /** Saves a document in the archive.
            * @param document A document
     * @return DocumentMetadata The meta data of the saved document
     */
    DocumentMetaData save(Document document);

    /**
     * Finds document in the archive matching the given parameter.
     * A list of document meta data which does not include the file data.
     * Use getDocumentFile and the id from the meta data to get the file.
     * Returns an empty list if no document was found.
     *
     * @param websiteID The id of a website, may be null
     * @return A list of document meta data
     */
    List<DocumentMetaData> findDocuments(int websiteID, int offset, int limit);

    /**
     * Returns the document file from the archive with the given id.
     * Returns null if no document was found.
     *
     * @param id The id of a document
     * @return A document file
     */
    byte[] getDocumentFile(String id);
}
