package com.spring.core.dao.impl;

import com.spring.core.dao.SystemParameterDAO;
import com.spring.core.entity.SystemParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: krissadewo
 * Date: 11/26/13
 * Time: 3:33 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class SystemParameterDAOImpl implements SystemParameterDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public SystemParameter get() {
        String sql = "SELECT * FROM system_parameter";
        return jdbcTemplate.queryForObject(sql, new SystemParameterRowMapper());
    }

    class SystemParameterRowMapper implements RowMapper<SystemParameter> {

        @Override
        public SystemParameter mapRow(ResultSet rs, int rowNum) throws SQLException {
            SystemParameter systemParameter = new SystemParameter();
            systemParameter.setMedia_Server(rs.getString("media_server"));
            return systemParameter;
        }
    }
}

