package com.spring.core.dao.impl;

import com.spring.core.dao.DocumentMetaDataDAO;
import com.spring.core.entity.DocumentMetaData;
import com.spring.core.util.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by godfather on 1/31/17.
 */
@Repository
public class DocumentMetaDataImpl implements DocumentMetaDataDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    @Transactional
    public DocumentMetaData save(final DocumentMetaData entity) {
        final String sql = "INSERT INTO " + Table.METADATA + "(" +
                "uuid, " +
                "file_name, " +
                "document_date, " +
                "website_id" + ")" +
                " VALUE (?,?,?,?)";

        jdbcTemplate.update(Connection -> {
                PreparedStatement ps = Connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, entity.getUuid());
                ps.setString(2, entity.getFileName());
                ps.setDate(3, new Date(entity.getDocumentDate().getTime()));
                ps.setString(4, String.valueOf(entity.getWebsiteId()));

                return ps;
            });

        return entity;
    }

    @Override
    public DocumentMetaData update(DocumentMetaData entity) {
        return null;
    }

    @Override
    public DocumentMetaData delete(DocumentMetaData entity) {
        return null;
    }

    @Override
    public DocumentMetaData findById(Long id) {
        return null;
    }

    @Override
    public DocumentMetaData findByParam(int user_id, int tryout_id) {
        return null;
    }

    @Override
    public List<DocumentMetaData> find(Integer offset, Integer limit) {
        return null;
    }

    @Override
    public int count(DocumentMetaData param) {
        return 0;
    }

    @Override
    public List<DocumentMetaData> findByWebsiteId(int websiteId, int offset, int limit) {
        String sql = "SELECT * FROM " + Table.METADATA +
                "WHERE website_id = ?";

        List<Object> params = new ArrayList<>();

        sql += " LIMIT ?, ?";
        params.add(offset);
        params.add(limit);

        return jdbcTemplate.query(sql, new DocumentRowMapper(), websiteId, offset, limit);
    }

    class DocumentRowMapper implements RowMapper<DocumentMetaData>{
        @Override
        public DocumentMetaData mapRow(ResultSet resultSet, int i) throws SQLException {
            DocumentMetaData documentMetaData = new DocumentMetaData();
            documentMetaData.setUuid(resultSet.getString("uuid"));
            documentMetaData.setFileName(resultSet.getString("file_name"));
            documentMetaData.setDocumentDate(resultSet.getTimestamp("document_date"));
            documentMetaData.setWebsiteId(resultSet.getInt("website_id"));
            return documentMetaData;
        }
    }
}
