package com.spring.core.util;

import com.spring.core.AppCore;
import com.spring.core.Profile;
import com.spring.core.common.Result;
import com.spring.core.common.TimeStampWrapper;
import org.jasypt.contrib.org.apache.commons.codec_1_3.binary.Base64;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.*;


/**
 * Created by sukenda on 01/04/16.
 */
public class ImageHelper {

    private Profile profile;

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    /**
     * Upload Image to server using Files Copy
     *
     * @param base64
     * @return
     */
    public Result uploadImageUsingFilesCopy(String base64) {
        Result result = new Result();
        try {
            String fileName = TimeStampWrapper.getTimeStamp() + ".jpg";

            Path filePath = Paths.get(profile.getImageDir().concat(fileName));
            ByteBuffer writeBuffer = ByteBuffer.wrap(Base64.decodeBase64(base64.getBytes()));

            FileChannel writeChannel = FileChannel.open(filePath, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
            writeChannel.write(writeBuffer); //write to system

            FileInputStream fileInputStream = new FileInputStream(profile.getImageDir().concat(fileName));
            Files.copy(fileInputStream, filePath, StandardCopyOption.REPLACE_EXISTING);

            result.setImageName(fileName);
            result.setMessage(Result.UPLOAD_SUCCESS);
        } catch (IOException e) {
            result.setMessage(Result.UPLOAD_FAILED + "-" + e.getMessage());
            AppCore.getLogger(this).error(e.getMessage());
        }

        return result;
    }

    /**
     * Delete file in server for problem if JDBC rollback and image upload success
     *
     * @param imageUrl
     */
    public Result deleteFile(String imageUrl) {
        Result result = new Result();
        try {
            Path filePath = Paths.get(profile.getImageDir().concat(imageUrl));
            Files.delete(filePath);

            result.setMessage(Result.DELETE_SUCCESS);
        } catch (Exception e) {
            AppCore.getLogger(this).debug(e.getMessage());
            result.setMessage(Result.DELETE_FAILED);
        }

        return result;
    }


    /**
     * compress image
     */

    public static String compressImage(String filePath, String fileName) {
        return null;
    }
}
