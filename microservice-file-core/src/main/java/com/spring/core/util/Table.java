package com.spring.core.util;

/**
 * Created by kris on 15/05/14.
 * Provide utility for DML,DDL
 */
public class Table {

    /**
     * All table name need separate with space at the end of sentence
     */
    public static final String MASTER_Client = "user ";

    public static final String SYSTEM_ROLE = "system_role ";
    public static final String SYSTEM_USER = "system_user ";
    public static final String SYSTEM_ROLE_USER = "system_role_user ";
    public static final String WEBSITE_INDEX = "index_website ";
    public static final String METADATA = "image_index ";

    public static final String LOG_USER = "log_user ";


}
