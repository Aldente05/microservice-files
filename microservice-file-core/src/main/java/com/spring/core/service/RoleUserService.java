/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.core.service;

import com.spring.core.dao.RoleUserDAO;
import com.spring.core.entity.RoleUser;
import com.spring.core.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author krissadewo
 */
@Service
public class RoleUserService extends BaseService {

    @Autowired
    private RoleUserDAO roleUserDAO;

    public List<RoleUser> find(User user) {
        return roleUserDAO.find(user);
    }

    public void deleteByUser(List<RoleUser> roleUsers) {
        roleUserDAO.delete(roleUsers);
    }

}
