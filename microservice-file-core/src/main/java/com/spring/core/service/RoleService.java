/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.core.service;

import com.spring.core.dao.RoleDAO;
import com.spring.core.entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author krissadewo
 */
@Service
public class RoleService {

    @Autowired
    private RoleDAO roleDAO;

    public List<Role> findAll() {
        return roleDAO.find(null, null);
    }
}
