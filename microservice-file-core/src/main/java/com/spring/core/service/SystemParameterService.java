package com.spring.core.service;

import com.spring.core.dao.SystemParameterDAO;
import com.spring.core.entity.SystemParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by f.putra on 10/20/16.
 */
@Service
public class SystemParameterService extends BaseService{

    @Autowired
    public SystemParameterDAO systemParameterDAO;

    public SystemParameter getImage(){
        return systemParameterDAO.get();
    }
}
