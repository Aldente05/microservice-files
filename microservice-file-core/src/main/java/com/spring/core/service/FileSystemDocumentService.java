package com.spring.core.service;

import com.spring.core.dao.DocumentDAO;
import com.spring.core.dao.DocumentMetaDataDAO;
import com.spring.core.entity.Document;
import com.spring.core.entity.DocumentMetaData;
import com.spring.core.util.DocumentHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by f.putra on 10/20/16.
 */
@Service
public class FileSystemDocumentService implements DocumentDAO {

    //    private static final Logger LOG = Logger.getLogger(FileSystemDocumentDao.class);
    @Autowired
    private DocumentMetaDataDAO documentMetaDataDAO;

    /**
     * Inserts a document to the archive by creating a folder with the UUID
     * of the document. In the folder the document is saved and a properties file
     * with the meta data of the document.
     */
    @Override
    public void insert(Document document) {
        try {
            saveFileData(document);
            documentMetaDataDAO.save(document.getMetadata());
        } catch (IOException e) {
            String message = "Error while inserting document";
//            LOG.error(message, e);
            throw new RuntimeException(message, e);
        }
    }

    /**
     * Finds documents in the data store matching the given parameter.
     * To find a document all document meta data sets are iterated to check if they match
     * the parameter.
     */
    @Override
    public List<DocumentMetaData> findByWebsiteId(int websiteID, int offset, int limit) {
        return documentMetaDataDAO.findByWebsiteId(websiteID, offset, limit);
    }

    /**
     * Returns the document from the data store with the given UUID.
     */
    @Override
    public Document load(String uuid) {
        try {
            Path path = Paths.get(new StringBuilder().append(DocumentHelper.DIRECTORY).append(File.separator).append(uuid).toString());
            Document document = new Document();
            document.setFileData(Files.readAllBytes(path));
            return document;
        } catch (IOException e) {
            String message = "Error while loading document with id: " + uuid;
//            LOG.error(message, e);
            throw new RuntimeException(message, e);
        }

    }

    private void saveFileData(Document document) throws IOException {
        String path = new StringBuilder().append(DocumentHelper.DIRECTORY).append(File.separator).append(document.getUuid()).toString();
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(path), Boolean.parseBoolean(document.getUuid())));
        stream.write(document.getFileData());
        stream.close();
    }

}
