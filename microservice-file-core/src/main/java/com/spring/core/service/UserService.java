/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.core.service;

import com.spring.core.AppCore;
import com.spring.core.Profile;
import com.spring.core.common.Constant;
import com.spring.core.common.Result;
import com.spring.core.dao.RoleUserDAO;
import com.spring.core.dao.UserDAO;
import com.spring.core.entity.RoleUser;
import com.spring.core.entity.User;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author kris
 */
@Service
public class UserService extends BaseService {

    @Autowired
    private UserDAO userDao;
    @Autowired
    private RoleUserDAO roleUserDAO;
    @Autowired(required = false)
    private Profile profile;

    public boolean isAuthenticatedUser() {
        if (profile.getName().equals(Profile.STATUS.DEV.toString())) {
            doLogin(new User(Constant.DEFAULT_USER, Constant.DEFAULT_PASSWORD));
        }

        return AppCore.getInstance().getUserFromSession() != null;
    }

    @Transactional
    public User doLogin(User user) {
        try {
            User currentUser = userDao.getByUsername(user.getUsername());
            if (currentUser != null) {
                StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
                if (passwordEncryptor.checkPassword(user.getPassword(), currentUser.getPassword())) {
                    AppCore.getInstance().getHttpSession().setAttribute(Constant.SESSION_USER, currentUser);
                    currentUser.setSession(AppCore.getInstance().getHttpServletRequest().getRequestedSessionId());
                    userDao.saveLogin(currentUser);
                    return user;
                }
            }
        } catch (DataAccessException e) {
            AppCore.getLogger(this).error(e.getMessage());
        }

        return null;
    }

    @Transactional
    public void doLogout() {
        User user = AppCore.getInstance().getUserFromSession();
        if (user == null) {
            AppCore.getLogger(this).info("session expired for " + AppCore.getInstance().getHttpServletRequest().getRequestedSessionId());
            User currentUser = userDao.getLogUser(AppCore.getInstance().getHttpServletRequest().getRequestedSessionId());
            if (currentUser != null) {
                userDao.saveLogout(currentUser);
            }
        } else {
            AppCore.getLogger(this).info("user " + user.getUsername() + " logout success");
            User currentUser = userDao.getLogUser(user.getId());
            userDao.saveLogout(currentUser);
        }

        AppCore.getInstance().getHttpServletRequest().getSession().invalidate();
    }

    public User getUserFromSession() {
        return AppCore.getInstance().getUserFromSession();
    }

    public List<User> getAll() {
        return userDao.find(null, null);
    }

    public Result save(final User user) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
                String encryptedPassword = passwordEncryptor.encryptPassword(user.getPassword());
                try {
                    if (user.getId() != null) {
                        String currentPassword = userDao.getPassword(user);
                        if (!currentPassword.equals(user.getPassword())) {
                            user.setPassword(encryptedPassword);
                        }

                        roleUserDAO.delete(roleUserDAO.find(user)); // delete all role user before update
                        userDao.update(user);

                        List<RoleUser> roleUsers = new ArrayList<>();
                        for (RoleUser roleUser : user.getRoleUsers()) { //set new user to role
                            roleUser.setUser(user);
                            roleUsers.add(roleUser);
                        }

                        roleUserDAO.save(roleUsers);
                    } else {
                        user.setPassword(encryptedPassword);
                        User resultUser = userDao.save(user); //save user

                        List<RoleUser> roleUsers = new ArrayList<>();
                        for (RoleUser roleUser : resultUser.getRoleUsers()) { //set new user to role
                            roleUser.setUser(resultUser);
                            roleUsers.add(roleUser);
                        }

                        roleUserDAO.save(roleUsers);
                    }
                } catch (DataAccessException dae) {
                    status.setRollbackOnly();
                    throw dae;
                }

                return null;
            }
        });
    }

    public Result delete(final User user) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    user.setDeleteBy(AppCore.getInstance().getUserFromSession().getId());
                    user.setDeleteTime(new Date());
                    userDao.delete(user);
                } catch (DataAccessException dae) {
                    status.setRollbackOnly();
                    throw dae;
                }

                return null;
            }
        });
    }
}
