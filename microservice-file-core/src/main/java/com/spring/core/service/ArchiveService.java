package com.spring.core.service;

import com.spring.core.dao.ArchiveDAO;
import com.spring.core.dao.DocumentDAO;
import com.spring.core.entity.Document;
import com.spring.core.entity.DocumentMetaData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by f.putra on 10/20/16.
 */
@Service
public class ArchiveService implements ArchiveDAO, Serializable {

    private static final long serialVersionUID = 8119784722798361327L;

    @Autowired
    private DocumentDAO documentDAO;

    /**
     * Saves a document in the archive.
     */
    @Override
    public DocumentMetaData save(Document document) {
        documentDAO.insert(document);
        return document.getMetadata();
    }

    /**
     * Finds document in the archive
     */
    @Override
    public List<DocumentMetaData> findDocuments(int websiteID, int offset, int limit) {
        return documentDAO.findByWebsiteId(websiteID, offset, limit);
    }

    /**
     * Returns the document file from the archive
     */
    @Override
    public byte[] getDocumentFile(String id) {
        Document document = documentDAO.load(id);
        if(document!=null) {
            return document.getFileData();
        } else {
            return null;
        }
    }
}
