/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.core.common;

import java.io.Serializable;

/**
 * @author krissadewo Used for messages to the end of user
 */
public class Result implements Serializable {

    public static final String DATA_NOT_VALID = "Data tidak valid";
    public static final String SYSTEM_EXCEPTION = "Kesalahan Sistem";
    public static final String DB_EXCEPTION = "Database Bermasalah";
    public static final String SAVE_FAILED = "Gagal menyimpan data";
    public static final String SAVE_TRANSAKSI_NOT_FOUND = "Transaksi Rekanan Tidak Ditemukan";
    public static final String SAVE_SUCCESS = "Data berhasil disimpan";
    public static final String SAVE_DATA_EXIST = "Data sudah ada";
    public static final String SAVE_EMPTY = "Data yang akan disimpan tidak lengkap";
    public static final String DELETE_SUCCESS = "Data berhasil dihapus";
    public static final String DELETE_FAILED = "Gagal menghapus data";
    public static final String BATCH_SAVE_FAILED = "Beberapa data yang dimasukan bermasalah";
    public static final String PASSWORD_OR_USER_NOT_REGISTERED = "User atau password tidak terdaftar";
    public static final String LOGIN_SUCCESS = "Login sukses";
    public static final String LOGIN_FAILED = "Username atau password tidak terdaftar";
    public static final String SESSION_EXPIRED = "Sesi anda telah bearkhir; Silahkan login kembali";
    public static final String DELETE_CHILD_EXIST = "Data telah memiliki hubungan dengan data lain. Hubungi admin anda... ";
    public static final String DATA_NOT_SELECTED = "Pilih data yang akan diproses terlebih dahulu";
    public static final String DATA_NOT_EXIST = "Data tidak ditemukan";
    public static final String SEARCH_NOT_HERE = "Pencariah dapat dilakukan pada grid header; silahkan lihat dokumentasi.. ";
    public static final String EDIT_NOT_AVAILABLE = "Ubah data tidak di ijinkan";
    public static final String UPLOAD_FAILED = "Gagal mengunggah data";
    public static final String UPLOAD_SUCCESS = "Data berhasil diunggah";
    public static final String TRANSAKSI_NOT_FOUND = "Data transaksi tidak ditemukan";
    public static final String BUTTON_NOT_WORKING = "Tombol ini tidak bekerja pada mode yang dijalankan";
    public static final String LOAD_MESSAGE = "Loading ...";
    public static final String PRESS_ENTER_FOR_SEARCH = "Tekan enter untuk pencarian";
    private static final long serialVersionUID = -6811456395579116710L;
    private Long id;
    private String noBukti;
    private String message;
    private String imageName;

    public Result() {
    }

    public Result(String message) {
        this.message = message;
    }

    public Result(String message, Long id) {
        this.message = message;
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNoBukti() {
        return noBukti;
    }

    public void setNoBukti(String noBukti) {
        this.noBukti = noBukti;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    @Override
    public String toString() {
        return "Result{" +
                "id=" + id +
                ", noBukti='" + noBukti + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
