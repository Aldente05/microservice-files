package com.spring.core.common;

import java.util.UUID;

/**
 * Created by KENDA on 5/26/2015.
 */
public class TimeStampWrapper {

    public static String getTimeStamp() {
        return UUID.randomUUID().toString();
    }
}
