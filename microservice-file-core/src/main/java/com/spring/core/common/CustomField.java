/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.core.common;

import java.io.Serializable;

/**
 * @author krissadewo
 *         <p/>
 *         This class handle for combo box that need to created label and label to the
 *         view
 */
public class CustomField implements Serializable {

 	private static final long serialVersionUID = 3303375556567645094L;
	private String value;
    private int valueInteger;
    private String label;

    public CustomField() {
    }

    public CustomField(String value) {
        this.value = value;
    }

    public CustomField(String valueString, String label) {
        this.value = valueString;
        this.label = label;
    }

    public CustomField(int valueInteger, String label) {
        this.valueInteger = valueInteger;
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getValueInteger() {
        return valueInteger;
    }

    public void setValueInteger(int valueInteger) {
        this.valueInteger = valueInteger;
    }

    @Override
    public String toString() {
        return "CustomField{" + "value=" + value + ", label=" + label + '}';
    }
}
