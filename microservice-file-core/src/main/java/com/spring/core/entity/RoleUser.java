package com.spring.core.entity;

import java.io.Serializable;

/**
 * @author krissadewo
 *         5/6/2014,10:27 AM
 */
public class RoleUser implements Serializable {

    private static final long serialVersionUID = -153758805335656075L;
    private Long id;
    private User user;
    private Role role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "RoleUser{" +
                "id=" + id +
                ", user=" + user +
                ", role=" + role +
                '}';
    }
}
