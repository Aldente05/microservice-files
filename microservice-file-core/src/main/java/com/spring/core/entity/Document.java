package com.spring.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Properties;

/**
 * Created by f.putra on 10/20/16.
 */
public class Document extends DocumentMetaData implements Serializable {

    private static final long serialVersionUID = 2004955454853853315L;

    private byte[] fileData;

    public Document( byte[] fileData, String fileName, Date documentDate, int website_id) {
        super(fileName, documentDate, website_id);
        this.fileData = fileData;
    }

    public Document(Properties properties) {
        super(properties);
    }

    public Document(DocumentMetaData metadata) {
        super(metadata.getUuid(), metadata.getFileName(), metadata.getDocumentDate(), metadata.getWebsiteId());
    }

    public Document() {

    }

    public byte[] getFileData() {
        return fileData;
    }
    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }

    public DocumentMetaData getMetadata() {
        return new DocumentMetaData(getUuid(), getFileName(), getDocumentDate(), getWebsiteId());
    }

}
