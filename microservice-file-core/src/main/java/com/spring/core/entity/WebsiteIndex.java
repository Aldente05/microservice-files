package com.spring.core.entity;

import java.io.Serializable;

/**
 * Created by f.putra on 12/10/16.
 */
public class WebsiteIndex implements Serializable {

    private static final long serialVersionUID = 2405553546567324981L;

    private int id;
    private Long websiteId;
    private String websiteName;
    private Long owner_id;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(Long websiteId) {
        this.websiteId = websiteId;
    }

    public String getWebsiteName() {
        return websiteName;
    }

    public void setWebsiteName(String websiteName) {
        this.websiteName = websiteName;
    }

    @Override
    public String toString() {
        return "WebsiteIndex{" +
                "id=" + id +
                ", websiteId=" + websiteId +
                ", websiteName='" + websiteName + '\'' +
                '}';
    }
}
