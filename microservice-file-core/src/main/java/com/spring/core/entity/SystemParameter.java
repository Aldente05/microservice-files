package com.spring.core.entity;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: krissadewo
 * Date: 11/26/13
 * Time: 3:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class SystemParameter implements Serializable {

    private static final long serialVersionUID = -5810894004862398878L;
    private String Media_Server;

    public String getMedia_Server() {
        return Media_Server;
    }

    public void setMedia_Server(String media_Server) {
        Media_Server = media_Server;
    }

    @Override
    public String toString() {
        return "SystemParameter{" +
                "Media_Server='" + Media_Server + '\'' +
                '}';
    }
}
