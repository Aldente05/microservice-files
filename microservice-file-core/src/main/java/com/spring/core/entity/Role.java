/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.core.entity;

import java.io.Serializable;

/**
 * @author krissadewo
 */
public class Role implements Serializable {

    private static final long serialVersionUID = -5683075595824364950L;
    private Long id;
    private String nama;

    public Role() {
    }

    public Role(Long id, String nama) {
        this.id = id;
        this.nama = nama;
    }

    public Role(String nama) {
        this.nama = nama;
    }

    public Role(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    @Override
    public String toString() {
        return "Role{" + "id=" + id + ", nama=" + nama + '}';
    }
}
