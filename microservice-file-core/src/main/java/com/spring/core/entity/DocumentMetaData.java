package com.spring.core.entity;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.UUID;

/**
 * Created by f.putra on 10/20/16.
 */

public class DocumentMetaData implements Serializable {

    static final long serialVersionUID = 7283287076019483950L;

//    private static final Logger LOG = Logger.getLogger(DocumentMetadata.class);

    private static final String PROP_UUID = "uuid";
    private static final String PROP_WEBSITE_ID = "website-id";
    private static final String PROP_FILE_NAME = "file-name";
    private static final String PROP_DOCUMENT_DATE = "document-date";

    private static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_PATTERN);

    protected String uuid;
    protected String fileName;
    protected Date documentDate = new Date();
    protected int websiteId;

    public DocumentMetaData(String fileName, Date documentDate, int websiteId) {
        this(UUID.randomUUID().toString(), fileName, documentDate,websiteId);
    }

    public DocumentMetaData(String uuid, String fileName, Date documentDate, int websiteId) {
        super();
        this.uuid = uuid;
        this.fileName = fileName;
        this.documentDate = documentDate;
        this.websiteId = websiteId;
    }

    public DocumentMetaData(Properties properties) {
        this(properties.getProperty(PROP_UUID),
                properties.getProperty(PROP_FILE_NAME),
                null,
                Integer.parseInt(properties.getProperty(PROP_WEBSITE_ID)));
        String dateString = properties.getProperty(PROP_DOCUMENT_DATE);
        if(dateString!=null) {
            try {
                this.documentDate = DATE_FORMAT.parse(dateString);
            } catch (ParseException e) {
//                LOG.error("Error while parsing date string: " + dateString + ", format is: yyyy-MM-dd" , e);
            }
        }
    }

    public DocumentMetaData() {

    }

    public String getUuid() {
        return uuid;
    }
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Date getDocumentDate() {
        return documentDate;
    }
    public void setDocumentDate(Date documentDate) {
        this.documentDate = documentDate;
    }

    public int getWebsiteId() {
        return websiteId;
    }
    public void setWebsiteId(int websiteId) {
        this.websiteId = websiteId;
    }
}
